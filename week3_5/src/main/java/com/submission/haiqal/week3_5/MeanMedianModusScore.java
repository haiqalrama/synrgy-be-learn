package com.submission.haiqal.week3_5;

import java.io.*;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class MeanMedianModusScore implements School{

    @Override
    public List<Integer> readCsv(String path) {
        List<Integer> list = new ArrayList<>();
        try(BufferedReader br = new BufferedReader(new FileReader(path))){
            String line = "";
            list = new ArrayList<>();
            while (br.ready()) {
                line = br.readLine();
                String[] data = line.split(";");
                for (int i = 1; i < data.length; i++) {
                    list.add(Integer.parseInt(data[i]));
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return Collections.unmodifiableList(list);
    }

    @Override
    public void generate(List<Integer> list){
        int sum = 0;
        for (int i : list) {
            sum += i;
        }
        double mean = (double) sum / list.size();
        double median = 0;
        if (list.size() % 2 == 0) {
            median = (list.get(list.size() / 2) + list.get(list.size() / 2 - 1)) / 2.;
        } else {
            median = list.get(list.size() / 2);
        }
        int modus = 0;
        int max = 0;
        for (int i : list) {
            int count = 0;
            for (int j : list) {
                if (i == j) {
                    count++;
                }
            }
            if (count > max) {
                max = count;
                modus = i;
            }
        }
        try (PrintWriter pw = new PrintWriter(new FileWriter("src/main/java/com/submission/haiqal/week3_5/data_sekolah_modus_median.txt"))) {
            pw.println("Berikut Hasil Pengolahan Nilai:");
            pw.println("\nBerikut hasil sebaran data nilai");
            pw.println("Mean: " + mean);
            pw.println("Median: " + median);
            pw.print("Modus: " + modus);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
