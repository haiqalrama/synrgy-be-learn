package com.submission.haiqal.week3_5;

import java.io.*;
import java.util.*;

public class FrequencyScore implements School{
    @Override
    public List<Integer> readCsv(String path) {
        List<Integer> list = new ArrayList<>();
        try (BufferedReader br = new BufferedReader(new FileReader(path))) {
            String line = "";
            list = new ArrayList<>();
            while (br.ready()) {
                line = br.readLine();
                String[] data = line.split(";");
                for (int i = 1; i < data.length; i++) {
                    list.add(Integer.parseInt(data[i]));
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return Collections.unmodifiableList(list);
    }

    @Override
    public void generate(List<Integer> list) {
        Map<String, Integer> map = new HashMap<>();
        map.put("kurang dari 6", 0);
        map.put("6", 0);
        map.put("7", 0);
        map.put("8", 0);
        map.put("9", 0);
        map.put("10", 0);

        for (int el : list) {
            if (el < 6) {
                map.put("kurang dari 6", map.get("kurang dari 6") + 1);
            } else if (el == 6) {
                map.put("6", map.get("6") + 1);
            } else if (el == 7) {
                map.put("7", map.get("7") + 1);
            } else if (el == 8) {
                map.put("8", map.get("8") + 1);
            } else if (el == 9) {
                map.put("9", map.get("9") + 1);
            } else if (el == 10) {
                map.put("10", map.get("10") + 1);
            }
        }

        try(PrintWriter pw = new PrintWriter(new FileWriter("src/main/java/com/submission/haiqal/week3_5/data_sekolah_modus.txt"))) {
            pw.println("Berikut Hasil Pengolahan Nilai:");
            pw.println("\nNilai\t\t\t|\tFrekuensi");
            pw.println("kurang dari 6\t|\t" + map.get("kurang dari 6"));
            pw.println("6\t\t\t\t|\t" + map.get("6"));
            pw.println("7\t\t\t\t|\t" + map.get("7"));
            pw.println("8\t\t\t\t|\t" + map.get("8"));
            pw.println("9\t\t\t\t|\t" + map.get("9"));
            pw.print("10\t\t\t\t|\t" + map.get("10"));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
