package com.submission.haiqal.week3_5;

import java.io.IOException;
import java.util.List;

public interface School {
    List<Integer> readCsv(String path);
    void generate(List<Integer> list);
}
