package com.submission.haiqal.week3_5;

import java.io.IOException;
import java.util.ArrayList;
import java.util.InputMismatchException;
import java.util.List;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner kb = new Scanner(System.in);
        School mm = new MeanMedianModusScore();
        School mm2 = new FrequencyScore();
        String path = "src/main/java/com/submission/haiqal/week3_5/data_sekolah.csv";
        int menu = -1;
        while(menu != 0) {
            System.out.println("--------------------------------------------------");
            System.out.println();
            System.out.println("Aplikasi Pengolahan Nilai Siswa");
            System.out.println("--------------------------------------------------");
            System.out.println("Letakan file csv dengan nama file data_sekolah di direktori berikut: src/main/java/com/submission/haiqal/week3_5/data_sekolah.csv");
            System.out.println();
            System.out.println("Pilih menu: ");
            System.out.println("1. Generate txt untuk menampilkan frekuensi nilai");
            System.out.println("2. Generate txt untuk menampilkan nilai rata-rata, median, dan modus");
            System.out.println("3. Generate kedua file");
            System.out.println("0. Exit");
            try {
                menu = kb.nextInt();
            } catch (InputMismatchException e) {
                System.out.println("Input harus angka");
                menu = -1;
                kb.nextLine();
                continue;
            }
            if (menu < 0 || menu > 3) {
                System.out.println("Input harus antara 1-3");
                menu = -1;
                continue;
            }
            List<Integer> list = new ArrayList<>();
            switch (menu) {
                case 1:
                    list = mm.readCsv(path);
                    mm.generate(list);
                    break;
                case 2:
                    list = mm2.readCsv(path);
                    mm2.generate(list);
                    break;
                case 3:
                    list = mm.readCsv(path);
                    mm.generate(list);
                    list = mm2.readCsv(path);
                    mm2.generate(list);
                    break;
            }
        }
    }
}
