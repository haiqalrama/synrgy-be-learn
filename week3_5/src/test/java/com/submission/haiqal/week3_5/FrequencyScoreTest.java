package com.submission.haiqal.week3_5;

import org.junit.jupiter.api.Test;

import java.util.Collections;

import static org.junit.jupiter.api.Assertions.*;

class FrequencyScoreTest {

    String successPath = "src/main/java/com/submission/haiqal/week3_5/data_sekolah.csv";
    String wrongPath = "src/week2_5/submission/data_sekolah_wrong.csv";
    FrequencyScore fs = new FrequencyScore();

    @Test
    void fileNotFound() {
        assertEquals(Collections.emptyList(),fs.readCsv(wrongPath));
    }

    @Test
    void fileFound() {
        assertNotEquals(Collections.emptyList(), fs.readCsv(successPath));
    }
}