package com.submission.haiqal.week3_5;

import org.junit.jupiter.api.Test;

import java.util.Collections;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;

class MeanMedianModusScoreTest {

    String successPath = "src/main/java/com/submission/haiqal/week3_5/data_sekolah.csv";
    String wrongPath = "src/week2_5/submission/data_sekolah_wrong.csv";
    MeanMedianModusScore mm = new MeanMedianModusScore();

    @Test
    void fileNotFound() {
        assertEquals(Collections.emptyList(),mm.readCsv(wrongPath));
    }

    @Test
    void fileFound() {
        assertNotEquals(Collections.emptyList(), mm.readCsv(successPath));
    }
}