package week2_1.assignment;

import java.util.Scanner;

public class JumlahAbjad {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        System.out.print("variable = ");
        String variable = input.nextLine().toLowerCase();

        int[] alphabet = new int[26];
        for (int i = 0; i < variable.length(); i++) {
            char c = variable.charAt(i);
            if (c >= 'a' && c <= 'z') {
                alphabet[c - 'a']++;
            }
        }
        for (int i = 0; i < alphabet.length; i++) {
            if (alphabet[i] != 0) {
                System.out.println("Alfabet " + (char) (i + 'a') + " muncul " + alphabet[i] + " kali");
            }
        }

        input.close();
    }
}
