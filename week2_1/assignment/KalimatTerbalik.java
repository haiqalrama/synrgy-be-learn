package week2_1.assignment;

import java.util.Scanner;

public class KalimatTerbalik {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        System.out.print("Masukkan kalimat: ");
        String kalimat = input.nextLine();
        String kalimatTerbalik = "";
        for (int i = kalimat.length() - 1; i >= 0; i--) {
            kalimatTerbalik += kalimat.charAt(i);
        }
        System.out.print("Kalimat terbalik = " + kalimatTerbalik);
        input.close();
    }
}
