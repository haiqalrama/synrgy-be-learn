package week2_4.submission.work2;

public class Main {
    public static void main(String[] args) {
        Account account = new Account( "Putra Hakim", "03425");
        System.out.println(account);

        System.out.println();

        account = new SavingsAccount("Putra Hakim", "03425", 250.45);
        System.out.println(account);
    }
}
