package week2_4.submission.work2;

public class SavingsAccount extends Account {
    private double balance;

    public SavingsAccount(String name, String accountNumber, double balance) {
        super(name, accountNumber);
        this.balance = balance;
    }

    public void deposit(double amount) {
        if (amount > 0) {
            balance += amount;
        } else {
            System.out.println("Invalid amount");
        }
    }

    public void withdraw(double amount) {
        if (amount > 0) {
            balance -= amount;
        } else {
            System.out.println("Invalid amount");
        }
    }

    @Override
    public String toString() {
        return super.getAccountNumber() + "\n" + super.getName() + "\n$" + balance;
    }
}
