package week2_4.submission.work1;

public class Main1 {
    public static void main(String[] args) {
        Hewan[] hewan = new Hewan[6];
        hewan[0] = new Dog();
        hewan[1] = new Spider();
        hewan[2] = new Fly();
        hewan[3] = new Centipede();
        hewan[4] = new Snake();
        hewan[5] = new Chicken();

        for (int i = 0; i < hewan.length; i++) {
            System.out.println(hewan[i].getClass().getSimpleName());
            System.out.println("Jumlah kaki: " + hewan[i].getNumberOfLegs());
            System.out.println("Makanan Favorit: " + hewan[i].getFavoriteFood());
            System.out.println();
        }
    }
}
