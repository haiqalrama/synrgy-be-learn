package week2_4.submission.work1;

public abstract class Hewan {
    protected int numberOfLegs;
    protected String favoriteFood;

    public Hewan(int numberOfLegs, String favoriteFood) {
        this.numberOfLegs = numberOfLegs;
        this.favoriteFood = favoriteFood;
    }

    public abstract int getNumberOfLegs();
    public abstract String getFavoriteFood();
}
