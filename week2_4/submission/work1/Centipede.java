package week2_4.submission.work1;

public class Centipede extends Hewan{

    public Centipede() {
        super(100, "Daun");
    }

    @Override
    public int getNumberOfLegs() {
        return this.numberOfLegs;
    }

    @Override
    public String getFavoriteFood() {
        return this.favoriteFood;
    }
}
