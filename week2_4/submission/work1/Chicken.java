package week2_4.submission.work1;

public class Chicken extends Hewan{

    public Chicken() {
        super(2, "Kacang");
    }

    @Override
    public int getNumberOfLegs() {
        return this.numberOfLegs;
    }

    @Override
    public String getFavoriteFood() {
        return this.favoriteFood;
    }
}
