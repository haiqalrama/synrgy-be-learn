package week2_4.submission.work1;

public class Spider extends Hewan{

    public Spider() {
        super(8, "Serangga");
    }

    @Override
    public int getNumberOfLegs() {
        return this.numberOfLegs;
    }

    @Override
    public String getFavoriteFood() {
        return this.favoriteFood;
    }
}
