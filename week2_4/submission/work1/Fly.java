package week2_4.submission.work1;

public class Fly extends Hewan{

    public Fly() {
        super(0, "Kotoran");
    }

    @Override
    public int getNumberOfLegs() {
        return this.numberOfLegs;
    }

    @Override
    public String getFavoriteFood() {
        return this.favoriteFood;
    }
}
