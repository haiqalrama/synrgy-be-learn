package week2_4.submission.work1;

public class Dog extends Hewan{

    public Dog() {
        super(4, "Daging");
    }

    @Override
    public int getNumberOfLegs() {
        return this.numberOfLegs;
    }

    @Override
    public String getFavoriteFood() {
        return this.favoriteFood;
    }
}
