package week2_4.submission.work1;

public class Snake extends Hewan{

    public Snake() {
        super(0, "Tikus");
    }

    @Override
    public int getNumberOfLegs() {
        return this.numberOfLegs;
    }

    @Override
    public String getFavoriteFood() {
        return this.favoriteFood;
    }
}
