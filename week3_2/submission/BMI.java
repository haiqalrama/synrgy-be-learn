package week3_2.submission;

import java.util.InputMismatchException;
import java.util.Scanner;

public class BMI {
    public static void main(String[] args) {

        Scanner kb = new Scanner(System.in);
        double tinggi = 0;
        double berat = 0;
        boolean apakahAngka = false;

        while (!apakahAngka) {
            try {
                System.out.print("Masukkan tinggi badan(cm): ");
                tinggi = kb.nextDouble();
                System.out.print("Masukkan berat badan(kg): ");
                berat = kb.nextDouble();
                apakahAngka = true;

                double bmi = berat / Math.pow(tinggi / 100, 2);
                if (bmi > 30) {
                    System.out.println("Anda obesitas");
                } else if (bmi > 25) {
                    System.out.println("Anda kelebihan berat badan");
                } else if (bmi > 18.5) {
                    System.out.println("Anda sehat");
                } else {
                    System.out.println("Anda kekurangan berat badan");
                }
            } catch (InputMismatchException e) {
                kb.nextLine();
                System.out.println("Masukkan input yang sesuai");
            }
        }
    }
}
